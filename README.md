# cron-parser
# How to run
`sbt "run $CRON"`
Where $CRON is a cron statement.
For example
```
sbt "run * * * * * /command"
sbt "run 1 1,10 1-10 1-5/2 * /command"
```
