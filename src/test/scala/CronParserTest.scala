import org.scalatest.FunSuite
import parser._

class CronParserTest extends FunSuite {
  test("all values parsed correctly") {
    val cron = Array("*", "*", "*", "*", "*", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1(MinuteField).sameElements((0 to 59).toArray.map(_.toString)))
    assert(parsedCron._1(HourField).sameElements((0 to 23).toArray.map(_.toString)))
    assert(parsedCron._1(DayOfTheMonthField).sameElements((1 to 31).toArray.map(_.toString)))
    assert(parsedCron._1(MonthField).sameElements((1 to 12).toArray.map(_.toString)))
    assert(parsedCron._1(DayOfTheWeekField).sameElements((1 to 7).toArray.map(_.toString)))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("single values parsed correctly") {
    val cron = Array("1", "5", "6", "10", "4", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1(MinuteField).sameElements(Array("1")))
    assert(parsedCron._1(HourField).sameElements(Array("5")))
    assert(parsedCron._1(DayOfTheMonthField).sameElements(Array("6")))
    assert(parsedCron._1(MonthField).sameElements(Array("10")))
    assert(parsedCron._1(DayOfTheWeekField).sameElements(Array("4")))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("list of values parsed correctly") {
    val cron = Array("1,2", "5,10,14", "6,1", "10,11,12", "4,7", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1(MinuteField).sameElements(Array("1", "2")))
    assert(parsedCron._1(HourField).sameElements(Array("5", "10", "14")))
    assert(parsedCron._1(DayOfTheMonthField).sameElements(Array("6", "1")))
    assert(parsedCron._1(MonthField).sameElements(Array("10", "11" , "12")))
    assert(parsedCron._1(DayOfTheWeekField).sameElements(Array("4", "7")))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("range parsed correctly") {
    val cron = Array("1-2", "1-20", "5-6", "5-10", "1-7", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1(MinuteField).sameElements((1 to 2).toArray.map(_.toString)))
    assert(parsedCron._1(HourField).sameElements((1 to 20).toArray.map(_.toString)))
    assert(parsedCron._1(DayOfTheMonthField).sameElements((5 to 6).toArray.map(_.toString)))
    assert(parsedCron._1(MonthField).sameElements((5 to 10).toArray.map(_.toString)))
    assert(parsedCron._1(DayOfTheWeekField).sameElements((1 to 7).toArray.map(_.toString)))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("range with step parsed correctly") {
    val cron = Array("1-2/2", "1-20/5", "5-6/1", "5-10/3", "1-7/2", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1(MinuteField).sameElements(Array("1")))
    assert(parsedCron._1(HourField).sameElements(Array("1", "6", "11", "16")))
    assert(parsedCron._1(DayOfTheMonthField).sameElements(Array("5", "6")))
    assert(parsedCron._1(MonthField).sameElements(Array("5", "8")))
    assert(parsedCron._1(DayOfTheWeekField).sameElements(Array("1", "3", "5", "7")))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("single value with step parsed correctly") {
    val cron = Array("1/10", "5/5", "6/7", "10/2", "4/1", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1(MinuteField).sameElements(Array("1", "11", "21", "31", "41", "51")))
    assert(parsedCron._1(HourField).sameElements(Array("5", "10", "15", "20")))
    assert(parsedCron._1(DayOfTheMonthField).sameElements(Array("6", "13", "20", "27")))
    assert(parsedCron._1(MonthField).sameElements(Array("10", "12")))
    assert(parsedCron._1(DayOfTheWeekField).sameElements(Array("4", "5", "6", "7")))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("all value with step parsed correctly") {
    val cron = Array("*/10", "*/5", "*/7", "*/2", "*/1", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1(MinuteField).sameElements(Array("0", "10", "20", "30", "40", "50")))
    assert(parsedCron._1(HourField).sameElements(Array("0", "5", "10", "15", "20")))
    assert(parsedCron._1(DayOfTheMonthField).sameElements(Array("1", "8", "15", "22", "29")))
    assert(parsedCron._1(MonthField).sameElements(Array("1", "3", "5", "7", "9", "11")))
    assert(parsedCron._1(DayOfTheWeekField).sameElements(Array("1", "2", "3", "4", "5", "6", "7")))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("non digit characters causes non-parsing of field") {
    val cron = Array("1F", "F", "1,2,F", "1-2/F", "2/F", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1.get(MinuteField).equals(Option.empty))
    assert(parsedCron._1.get(HourField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheMonthField).equals(Option.empty))
    assert(parsedCron._1.get(MonthField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheWeekField).equals(Option.empty))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("invalid characters in range causes non-parsing of field") {
    val cron = Array("F-F", "F-1", "1-F", "6-5", "-", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1.get(MinuteField).equals(Option.empty))
    assert(parsedCron._1.get(HourField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheMonthField).equals(Option.empty))
    assert(parsedCron._1.get(MonthField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheWeekField).equals(Option.empty))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("missing characters causes non-parsing of field") {
    val cron = Array("1,", ",1", "2/", "1-", "-1", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1.get(MinuteField).equals(Option.empty))
    assert(parsedCron._1.get(HourField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheMonthField).equals(Option.empty))
    assert(parsedCron._1.get(MonthField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheWeekField).equals(Option.empty))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("single value outside min/max causes non-parsing of field") {
    val cron = Array("60", "30", "0", "13", "100", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1.get(MinuteField).equals(Option.empty))
    assert(parsedCron._1.get(HourField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheMonthField).equals(Option.empty))
    assert(parsedCron._1.get(MonthField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheWeekField).equals(Option.empty))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("list of values outside min/max causes non-parsing of field") {
    val cron = Array("1,60", "1,30", "1,0", "1,13", "1,100", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1.get(MinuteField).equals(Option.empty))
    assert(parsedCron._1.get(HourField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheMonthField).equals(Option.empty))
    assert(parsedCron._1.get(MonthField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheWeekField).equals(Option.empty))
    assert(parsedCron._2.equals(cron(5)))
  }

  test("range outside min/max causes non-parsing of field") {
    val cron = Array("1-60", "20-30", "0-31", "0-13", "5-100", "/usr/bin/find")
    val parsedCron = CronParser.parseCron(cron)

    assert(parsedCron._1.get(MinuteField).equals(Option.empty))
    assert(parsedCron._1.get(HourField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheMonthField).equals(Option.empty))
    assert(parsedCron._1.get(MonthField).equals(Option.empty))
    assert(parsedCron._1.get(DayOfTheWeekField).equals(Option.empty))
    assert(parsedCron._2.equals(cron(5)))
  }
}
