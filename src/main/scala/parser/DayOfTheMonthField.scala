package parser

object DayOfTheMonthField extends CronField {
  override val MaxValue: Int = 31
  override val MinValue: Int = 1
  override val Name: String = "day of month"
}
