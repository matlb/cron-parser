package parser

import scala.util.{Try,Success,Failure}

object CronParser {
  def parseCron(cron: Array[String]): (Map[CronField, Array[String]], String) = {
    if (cron.length != 6) {
      return (Map(), "")
    }

    val cronFields = Map(
      MinuteField -> cron(0),
      HourField -> cron(1),
      DayOfTheMonthField -> cron(2),
      MonthField -> cron(3),
      DayOfTheWeekField -> cron(4)
    )

    val parsedCronFields: Map[CronField, Array[String]] = cronFields.flatMap(cronField => {
      val parsedCron = Try(parseCronFields(cronField._1, cronField._2))
      parsedCron match {
        case Success(array) => Map(cronField._1 -> parsedCron.get)
        case Failure(f) => None
      }
    })

    (parsedCronFields, cron(5))
  }

  private def parseCronFields(cronField: CronField, cronStatement: String): Array[String] = {
    val parsedCronField: Array[String] = cronStatement.split(",", -1).flatMap(entry => {
      if (entry.contains("/")) {
        val slashSplit = entry.split("/")
        val range = cronField.checkForRange(slashSplit(0), cronField.rangeUsingValue, cronField.rangeFromValue)
        cronField.applyStepToRange(range, slashSplit(1))
      } else {
        cronField.checkForRange(entry, cronField.rangeUsingValue, cronField.checkForAllValue)
      }
    })
    parsedCronField
  }
}
