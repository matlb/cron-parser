package parser

object DayOfTheWeekField extends CronField {
  override val MaxValue: Int = 7
  override val MinValue: Int = 1
  override val Name: String = "day of week"
}