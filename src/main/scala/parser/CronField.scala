package parser

trait CronField {
  val MaxValue: Int
  val MinValue: Int
  val Name: String

  def checkForRange(value: String, withRange: (String) => Array[String], withoutRange: (String) => Array[String]): Array[String] = {
    if (value.contains("-")) {
      return withRange(value)
    }
    withoutRange(value)
  }

  def checkForAllValue(value: String): Array[String] = {
    if (value.equals("*")) {
      return rangeFromValue(MinValue.toString)
    }
    if (value.toInt >= MinValue && value.toInt <= MaxValue) {
      return Array(value)
    }
    throw new IllegalStateException
  }

  def applyStepToRange(range: Array[String], step: String): Array[String] = {
    range.zipWithIndex.filter(_._2 % step.toInt == 0).map(_._1)
  }

  def rangeUsingValue(value: String): Array[String] = {
    val dashSplit = value.split("-")
    val from = dashSplit(0).toInt
    val to = dashSplit(1).toInt

    if (from >= MinValue && to <= MaxValue && from <= to) {
      return (from to to).toArray.map(_.toString)
    }
    throw new IllegalStateException
  }

  def rangeFromValue(value: String): Array[String] = {
    if (value.equals("*")) {
      return rangeFromValue(MinValue.toString)
    }
    (value.toInt to MaxValue).toArray.map(_.toString)
  }
}
