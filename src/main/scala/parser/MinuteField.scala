package parser

object MinuteField extends CronField {
  override val MaxValue: Int = 59
  override val MinValue: Int = 0
  override val Name: String = "minute"
}
