package parser

object HourField extends CronField {
  override val MaxValue: Int = 23
  override val MinValue: Int = 0
  override val Name: String = "hour"
}
