package parser

object MonthField extends CronField {
  override val MaxValue: Int = 12
  override val MinValue: Int = 1
  override val Name: String = "month"
}
