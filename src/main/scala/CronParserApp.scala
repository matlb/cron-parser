import parser._

object CronParserApp {
  def main(args: Array[String]): Unit = {
    val parsedCron = CronParser.parseCron(args)
    printCronFields(parsedCron._1, parsedCron._2)
  }

  private def printCronFields(parsedCronFields: Map[CronField, Array[String]], command: String) = {
    println("%s\t\t %s".format(MinuteField.Name, parsedCronFields.getOrElse(MinuteField, Array("Invalid cron field")).mkString(" ")))
    println("%s\t\t %s".format(HourField.Name, parsedCronFields.getOrElse(HourField, Array("Invalid cron field")).mkString(" ")))
    println("%s\t %s".format(DayOfTheMonthField.Name,parsedCronFields.getOrElse(DayOfTheMonthField, Array("Invalid cron field")).mkString(" ")))
    println("%s\t\t %s".format(MonthField.Name, parsedCronFields.getOrElse(MonthField, Array("Invalid cron field")).mkString(" ")))
    println("%s\t %s".format(DayOfTheWeekField.Name, parsedCronFields.getOrElse(DayOfTheWeekField, Array("Invalid cron field")).mkString(" ")))
    println("command\t\t %s".format(if (command.isEmpty) "Invalid cron field" else command))
  }
}
